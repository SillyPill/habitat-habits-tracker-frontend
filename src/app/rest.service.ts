import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})

export class RestService {
	readonly endpoint = 'http://localhost:8000/';
	readonly httpOptions = {
		headers: new HttpHeaders({
'Content-Type':  'application/json',
		})
	};

	constructor(private http: HttpClient) {
	}
	private extractData(res: Response) {
		let body = res;
		return body || { };
	}

	createUser(user): Observable<any> {
		return this.http.post<any>( this.endpoint + 'register', JSON.stringify(user), this.httpOptions).pipe(
			tap((user) => console.log('success')),
			catchError(this.handleError<any>('createUser'))
		);
	}

	private handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			console.log(`${operation} failed: ${error.message}`);

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}


	login(creds): Observable<any> {
		return this.http.post<any>( this.endpoint + 'login', JSON.stringify(creds), this.httpOptions).pipe(
			tap((creds) => console.log('success')),
			catchError(this.handleError<any>('login'))
		);
	}

	getTodaysHabits(): Observable<any> {
		return this.http.get<any>( this.endpoint + 'api/today', this.httpOptions).pipe(
			tap((resp) => console.log('success')),
			catchError(this.handleError<any>('today\'s list'))
		);
	}

	addHabit(habit): Observable<any> {
		return this.http.post<any>( this.endpoint + 'api/add', JSON.stringify(habit), this.httpOptions).pipe(
			tap((habit) => console.log('success')),
			catchError(this.handleError<any>('add-habit'))
		);
	}

	markDone( id ): Observable<any> {
		return this.http.get<any>( this.endpoint + 'api/today/' + id + '/yes', this.httpOptions).pipe(
			tap((resp) => console.log('success')),
			catchError(this.handleError<any>('Mark Done'))
		);
	}

	habitData( id ): Observable<any> {
		return this.http.get<any>( this.endpoint + 'api/data/habit/' + id + '/yes', this.httpOptions).pipe(
			tap((resp) => console.log('success')),
			catchError(this.handleError<any>('Mark Done'))
		);
	}

	habitStrength( id ): Observable<any> {
		return this.http.get<any>( this.endpoint + 'api/data/habit/strength/' + id, this.httpOptions).pipe(
			tap((resp) => console.log('success')),
			catchError(this.handleError<any>('Mark Done'))
		);
	}

	getMyHabits(): Observable<any> {
		return this.http.get<any>( this.endpoint + 'api/habits', this.httpOptions).pipe(
			tap((resp) => console.log('success')),
			catchError(this.handleError<any>('today\'s list'))
		);
	}

	qod(): Observable<any> {
		return this.http.get<any>( 'https://quotes.rest/qod?category=inspire' ).pipe(
			tap((resp) => console.log('success')),
			catchError(this.handleError<any>('qod'))
		);
	}


}

import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RestService } from '../rest.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-manage-habits',
  templateUrl: './manage-habits.component.html',
  styleUrls: ['./manage-habits.component.scss']
})

export class ManageHabitsComponent implements OnInit {
	allHabits : any [];
	isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
		.pipe(
			map(result => result.matches)
		);

	constructor(private breakpointObserver: BreakpointObserver, public rest:RestService, public snackBar: MatSnackBar ) {}

	habitStrength( hid ){
		this.rest.habitStrength(hid).subscribe( (resp) => {
			if ( resp.message === "success" ) {
				this.snackBar.open('Habit strength is : '+resp.strenght , 'DISMISS')
			}
			else{
				console.log(resp);
			}

		})

	}


	ngOnInit() {
		this.rest.getMyHabits().subscribe((resp) => {
			console.log(resp);
			this.allHabits = resp;
			console.log(this.allHabits);
		})

	}

}

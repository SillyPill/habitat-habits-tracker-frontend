import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss'],
})
export class MainNavComponent {
  loggedin$: Observable<boolean> = of(!!localStorage.token);
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private router: Router, public snackBar: MatSnackBar) {}
  logout() {
  	localStorage.removeItem('token');
  	localStorage.removeItem('username');
	this.snackBar.open('You have successfully logged out', 'DISMISS');
	this.router.navigate(['']);
  }

}

import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';


@NgModule({
  imports: [MatSnackBarModule, MatButtonModule, MatCheckboxModule, MatCardModule, MatInputModule, MatExpansionModule],
  exports: [MatSnackBarModule, MatButtonModule, MatCheckboxModule, MatCardModule, MatInputModule, MatExpansionModule],
})
export class MaterialModule{ }


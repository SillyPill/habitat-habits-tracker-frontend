import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RestService } from '../rest.service';
import { MatSnackBar } from '@angular/material';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

	userName = localStorage.username;
	qod : any;
	todayHabits : any [];
	isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
		.pipe(
			map(result => result.matches)
		);

	constructor(private breakpointObserver: BreakpointObserver, public rest:RestService, public snackBar: MatSnackBar ) {}

	check(habit : any, i: number) {
		var habit_id = habit.id;
		this.rest.markDone(habit_id).subscribe( (resp) => {
			if ( resp.message === "success" ) {
				this.snackBar.open('Completed \''+habit.name+'\' successfully', 'DISMISS')
				this.todayHabits.splice(i, 1);
			}
			else{
				console.log(resp);
			}

		})

	}

	ngOnInit() {


		this.rest.getTodaysHabits().subscribe((resp) => {
			console.log(resp);
			this.todayHabits = resp;
			console.log(this.todayHabits);
		})


	}

}

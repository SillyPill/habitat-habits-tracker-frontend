import { Component, OnInit } from '@angular/core';
import { FormControl, Validators} from '@angular/forms';
import { RestService } from '../rest.service';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

	unameFormControl = new FormControl('', [
		Validators.required,
	]);

	emailFormControl = new FormControl('', [
		Validators.required,
		Validators.email,
	]);

	passwordFormControl = new FormControl('', [
		Validators.required
	]);


	constructor(public rest:RestService, private router: Router, public snackBar: MatSnackBar) { }
	ngOnInit() {
	}
	register() {
		var uname = this.unameFormControl.value;
		var email = this.emailFormControl.value;
		var passw = this.passwordFormControl.value;
		var bod = {
			"email" : email,
			"name" : uname,
			"password" : passw
		};
		this.rest.createUser( bod ).subscribe((resp) => {
		if ( resp.message === "success" ) {
		this.snackBar.open('User successfully created. Login to contiue', 'LOGIN').afterDismissed
().subscribe(() =>{
			this.router.navigate(['/login']);
		});
		}
		else{
			console.log(resp);
		}
		
		});

	}

}
